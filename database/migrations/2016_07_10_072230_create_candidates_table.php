<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_committee')->unsigned();
            $table->foreign('id_committee')->references('id')->on('committees');
            $table->integer('id_department')->unsigned();
            $table->foreign('id_department')->references('id')->on('departments');
            $table->integer('id_document')->unsigned();
            $table->foreign('id_document')->references('id')->on('documents');
            $table->string('documentNumber');
            $table->string('name');
            $table->integer('votesRecieved');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('candidates');
    }
}
