-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-07-2016 a las 22:32:29
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `challengetelus`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `candidates`
--

CREATE TABLE `candidates` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_committee` int(10) UNSIGNED NOT NULL,
  `id_department` int(10) UNSIGNED NOT NULL,
  `id_document` int(10) UNSIGNED NOT NULL,
  `documentNumber` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `votesRecieved` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `committees`
--

CREATE TABLE `committees` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_contry` int(10) UNSIGNED NOT NULL,
  `committeeName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `committees`
--

INSERT INTO `committees` (`id`, `id_contry`, `committeeName`, `created_at`, `updated_at`) VALUES
(1, 9, 'Technology', '2016-07-10 06:00:00', '2016-07-10 06:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contries`
--

CREATE TABLE `contries` (
  `id` int(10) UNSIGNED NOT NULL,
  `contryName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `contries`
--

INSERT INTO `contries` (`id`, `contryName`, `created_at`, `updated_at`) VALUES
(1, 'Canada', '2016-07-10 14:52:18', '2016-07-10 14:52:18'),
(6, 'Unite States', '2016-07-10 22:31:12', '2016-07-10 22:31:12'),
(7, 'Mexico', '2016-07-10 22:32:22', '2016-07-10 22:32:22'),
(8, 'Guatemala', '2016-07-10 22:34:25', '2016-07-10 22:34:25'),
(9, 'El Salvador', '2016-07-10 22:35:53', '2016-07-10 22:35:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `departmentName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_contry` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `departments`
--

INSERT INTO `departments` (`id`, `departmentName`, `id_contry`, `created_at`, `updated_at`) VALUES
(1, 'San Salvador', 9, '2016-07-10 06:00:00', '2016-07-10 06:00:00'),
(2, 'Santa Ana', 9, '2016-07-11 01:41:45', '2016-07-11 01:41:45'),
(3, 'San Miguel', 9, '2016-07-11 01:42:29', '2016-07-11 01:42:29'),
(4, 'Montreal', 1, '2016-07-11 01:43:47', '2016-07-11 01:43:47'),
(5, 'Monte Rey', 7, '2016-07-11 01:46:38', '2016-07-11 01:46:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documents`
--

CREATE TABLE `documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `documentName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `documents`
--

INSERT INTO `documents` (`id`, `documentName`, `created_at`, `updated_at`) VALUES
(1, 'passport', '2016-07-09 06:00:00', '2016-07-09 06:00:00'),
(2, 'Dui', '2016-07-11 00:47:30', '2016-07-11 00:47:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_07_10_070435_create_contries_table', 2),
('2016_07_10_070648_create_documents_table', 3),
('2016_07_10_070839_create_roles_table', 4),
('2016_07_10_071114_create_departments_table', 5),
('2016_07_10_071641_create_visits_table', 6),
('2016_07_10_072030_create_committees_table', 7),
('2016_07_10_072230_create_candidates_table', 8),
('2016_07_10_073548_create_uCV_table', 9),
('2016_07_10_080914_add_to_users_table', 10),
('2016_07_10_081222_add_more_to_users_table', 11),
('2016_07_10_084112_insert_data', 12),
('2016_07_10_084910_insert_data_contries', 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `rol` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `rol`, `created_at`, `updated_at`) VALUES
(1, 1, '2016-07-09 06:00:00', '2016-07-09 06:00:00'),
(2, 1, '2016-07-10 14:45:02', '2016-07-10 14:45:02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usercommitteevotes`
--

CREATE TABLE `usercommitteevotes` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_committee` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_document` int(10) UNSIGNED NOT NULL,
  `documentNumber` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `id_rol` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `id_document`, `documentNumber`, `id_rol`) VALUES
(1, 'Gabriel', 'gabo@gmail.com', '$2y$10$bZtrKplQmmPiWAv4bSNHI.UFbt6INX38bIaAyYJ.vQfkGpPUdza4q', '08lFG9hhZ11l0ZeAqFs4doZULsBfPTeh1MfO1VEnwE5ro5rLOeo82G6RHbex', '2016-07-09 06:00:00', '2016-07-10 22:59:06', 1, '', 1),
(4, 'joe', 'jose@yahoo.es', '$2y$10$25nsBFeKdbQlWcRI4eqiO.K862sJ.5zBX1yPyy0u6DFrkesVc5PuC', 'hGZpAZTvG4mh60ahSYSUPidOcAch7mERdJi2OQi3XFhkNSBSgeb6Xb6VcP2d', '2016-07-10 23:13:48', '2016-07-11 02:22:01', 1, '4455666', 2),
(5, 'Abigail', 'abi@yahoo.es', '$2y$10$phPNhlMY06wp1xoXjSMwveQap10/XSvdQn1S9/xTpTjnOvbEpJnNm', 'PxgQGKAB9sRIhR7IItGD4fIw4wub5erOritF3qCBAFMJLQcE0bnqYP0V3ePI', '2016-07-10 23:17:31', '2016-07-11 00:27:06', 1, '123457', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visits`
--

CREATE TABLE `visits` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `count` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `candidates`
--
ALTER TABLE `candidates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `candidates_id_committee_foreign` (`id_committee`),
  ADD KEY `candidates_id_department_foreign` (`id_department`),
  ADD KEY `candidates_id_document_foreign` (`id_document`);

--
-- Indices de la tabla `committees`
--
ALTER TABLE `committees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `committees_id_contry_foreign` (`id_contry`);

--
-- Indices de la tabla `contries`
--
ALTER TABLE `contries`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `departments_id_contry_foreign` (`id_contry`);

--
-- Indices de la tabla `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usercommitteevotes`
--
ALTER TABLE `usercommitteevotes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usercommitteevotes_id_committee_foreign` (`id_committee`),
  ADD KEY `usercommitteevotes_id_user_foreign` (`id_user`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_id_document_foreign` (`id_document`),
  ADD KEY `users_id_rol_foreign` (`id_rol`);

--
-- Indices de la tabla `visits`
--
ALTER TABLE `visits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `visits_id_user_foreign` (`id_user`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `candidates`
--
ALTER TABLE `candidates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `committees`
--
ALTER TABLE `committees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `contries`
--
ALTER TABLE `contries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `usercommitteevotes`
--
ALTER TABLE `usercommitteevotes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `visits`
--
ALTER TABLE `visits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `candidates`
--
ALTER TABLE `candidates`
  ADD CONSTRAINT `candidates_id_committee_foreign` FOREIGN KEY (`id_committee`) REFERENCES `committees` (`id`),
  ADD CONSTRAINT `candidates_id_department_foreign` FOREIGN KEY (`id_department`) REFERENCES `departments` (`id`),
  ADD CONSTRAINT `candidates_id_document_foreign` FOREIGN KEY (`id_document`) REFERENCES `documents` (`id`);

--
-- Filtros para la tabla `committees`
--
ALTER TABLE `committees`
  ADD CONSTRAINT `committees_id_contry_foreign` FOREIGN KEY (`id_contry`) REFERENCES `contries` (`id`);

--
-- Filtros para la tabla `departments`
--
ALTER TABLE `departments`
  ADD CONSTRAINT `departments_id_contry_foreign` FOREIGN KEY (`id_contry`) REFERENCES `contries` (`id`);

--
-- Filtros para la tabla `usercommitteevotes`
--
ALTER TABLE `usercommitteevotes`
  ADD CONSTRAINT `usercommitteevotes_id_committee_foreign` FOREIGN KEY (`id_committee`) REFERENCES `committees` (`id`),
  ADD CONSTRAINT `usercommitteevotes_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_id_document_foreign` FOREIGN KEY (`id_document`) REFERENCES `documents` (`id`),
  ADD CONSTRAINT `users_id_rol_foreign` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id`);

--
-- Filtros para la tabla `visits`
--
ALTER TABLE `visits`
  ADD CONSTRAINT `visits_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
