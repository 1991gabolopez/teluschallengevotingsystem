@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading"></div>

                    <div class="panel-body" align="center">
                        <h3>There is a vote for this committee and candidate! </h3>
                        <a href="{{url('/chooseCommittee')}}" class="btn btn-primary btn-lg btn-plus add-task">Back to committee list</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
