@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Reports</div>
                    <div class="panel-body">
                        <!-- Table-to-load-the-data Part -->
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Votes</th>
                            </tr>
                            </thead>
                            <tbody id="report-list" name="report-list">
                            @foreach ($datas as $data)
                                <tr>
                                    <td>{{$data->name}}</td>
                                    <td>{{$data->votesRecieved}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection