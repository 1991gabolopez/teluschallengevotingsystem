@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Online Voting</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/candidatesList') }}">
                            {{ csrf_field() }}
                            @if(Auth::check())
                                @if(Auth::user()->id_rol == 1)
                                    <div class="form-group">
                                        <label for="name" class="col-md-4 control-label">Choose a committee</label>
                                        <div class="col-md-6">
                                            <select class="form-control" name="id_committee">
                                                @foreach ($committees as $committee)
                                                    <option value="{{$committee->id}}">{{$committee->committeeName}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary" id="bnt">
                                                <i class="fa fa-btn fa-user"></i> Select
                                            </button>
                                        </div>
                                    </div>
                                @endif
                            @endif
                        </form>
                        <br/>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

