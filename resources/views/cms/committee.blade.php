@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                @if(Session::has('message'))

                    <div class="alert alert-{{Session::get('tipo')}} alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel-body">
                    <!-- Table-to-load-the-data Part -->
                    <a href="{{url('/committees/register')}}" class="btn btn-primary btn-xs btn-plus add-task">Add</a>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Id Contry</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                        <tbody id="committee-list" name="committee">
                        @foreach ($committees as $committee)
                        <tr id="committee{{$committee->id}}">
                            <td>{{$committee->id}}</td>
                            <td>{{$committee->committeeName}}</td>
                            <td>{{$committee->contryName}}</td>
                            <td>
                                {!!Form::open(['route'=>['Committees.destroy', $committee->id], 'method'=>'DELETE'])!!}
                                {!!Form::submit('Eliminar ', ['class'=>'btn btn-danger'])!!}
                                {!!Form::close()!!}
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection