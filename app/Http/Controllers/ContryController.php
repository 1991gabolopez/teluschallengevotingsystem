<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Contrie;
use Illuminate\Support\Facades\DB;
use Session;
class ContryController extends Controller
{

    public function getContry(){
        $contries = Contrie::all();

        return view('cms.contry', compact('contries'));
    }

    public function getContryRegister(){
        return view('cms.createContry');
    }

    public function setContry(){

        $data = request()->only('contryName');
        Contrie::create($data);

        Session::flash('tipo','success');
        Session::flash('message', 'Pais  registrado exitosamente');
        return redirect()->to('/contries');
    }

    public function deleteContry($id)
    {

        $Ncontry= DB::table('departments')
            ->select(DB::raw('count(*) as N'))
            ->where('id_contry', $id)
            ->get();

        if($Ncontry[0]->N ==0){

            Session::flash('tipo','success');
            Session::flash('message', 'Pais eliminado exitosamente');
            Contrie::destroy($id);
        }else{
            Session::flash('tipo','danger');
            Session::flash('message', 'Pais  no pudo ser eliminado');
        }

        return redirect('/contries');
    }
}
