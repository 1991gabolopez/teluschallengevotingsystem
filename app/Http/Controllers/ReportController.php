<?php

namespace App\Http\Controllers;

use App\Candidate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;

class ReportController extends Controller
{
    function getReport(){
        $datas = DB::table('candidates')->select('name', 'votesRecieved')->get();
        //var_dump($reports);
        return view('frontend.report', compact('datas'));
    }
}
