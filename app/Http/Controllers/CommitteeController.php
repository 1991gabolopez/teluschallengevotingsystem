<?php

namespace App\Http\Controllers;

use App\Committee;
use Illuminate\Http\Request;

use App\Http\Requests;

class CommitteeController extends Controller
{
    public function getCommittee(){
        $committees = Committee::all();

        return view('cms.committee', compact('committees'));
    }

    public function getCommitteeRegister(){
        $contries = Contrie::all();
        return view('cms.committeeRegister', compact('contries'));
    }

    public function setCommittee(){
        $data = request()->all();
        Committee::create($data);
        return redirect()->to('/committees');
    }
}
