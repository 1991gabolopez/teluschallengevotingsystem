<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contrie;
use App\Committee;
use App\Department;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Session;
class CommitteesController extends Controller
{
    public function getCommittee(){
        //$committees = Committee::all();
        $committees = DB::table('committees')
            ->join('contries', 'committees.id_contry', '=', 'contries.id')
            ->select('contries.contryName','committees.*')->get();
        return view('cms.committee', compact('committees'));
    }

    public function getCommitteeRegister(){
        $contries = Contrie::all();
        return view('cms.committeeRegister', compact('contries'));
    }

    public function setCommittee(){
        $data = request()->all();
        Committee::create($data);
        Session::flash('tipo','success');
        Session::flash('message', 'Comite creado exitosamente');
        return redirect()->to('/committees');
    }

    public function destroy($id){
            Committee::destroy($id);

        $committees = Committee::all();
        Session::flash('tipo','success');
        Session::flash('message', 'Comite Eliminado exitosamente');
        return view('cms.committee', compact('committees'));
    }
}
