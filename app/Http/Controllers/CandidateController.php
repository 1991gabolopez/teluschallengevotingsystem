<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Committee;
use App\Contrie;
use App\Document;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Illuminate\View\View;
use Mockery\CountValidator\Exception;

class CandidateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function registerCandidate()
    {
        //
        $committee = Committee::lists('committeeName','id');
        $country   = Contrie::all('id','contryName');
        $document  = Document::lists('documentName','id');

        return view('cms.candidate')->with([
            'committe'=>$committee,
            'country'=>$country,
            'document'=>$document]);
    }

    public function UpdateCandidate($id)
    {
        //

        $committee = Committee::all();
        $country   = Contrie::pluck('id','contryName');
        $document  = Document::pluck(['id','departmentName']);
        $department = DB::table('departments')
            ->join('contries', 'departments.id_contry', '=', 'contries.id')
            ->select('contries.contryName','departments.*')->get();

        $candidate = DB::table('candidates')
                      ->join('departments', 'candidates.id_department','=','departments.id')
                      ->join('contries','departments.id','=','contries.id')
                       ->where('candidates.id','=', $id)
                       ->select('candidates.*','countries.id as idCountry')->get();

        $department = DB::table('departments')
            ->join('contries', 'departments.id_contry', '=', 'contries.id')
            ->where('departments.id','=', $candidate->id_department)
            ->select('contries.contryName','departments.*')->get();


        return view('cms.candidate')->with([
            'candidate'=>$candidate,
            'committe'=>$committee,
            'country'=>$country,
            'department' =>$department,
            'document'=>$document]);
    }
    /** UpdateCandidate
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try{
        $candidate = new Candidate();
        $candidate = $request->all();
            $candidate->votesRecieved = 0;
            $candidate->save();
            Session::flash('tipo','success');
            Session::flash('message', 'Candidato added success');
            return View('cms.candidate');
        }
        catch (Exception $e){

            Session::flash('tipo','failure');
            Session::flash('message', 'failure to register candidate');
            return View('cms.candidate');
        }


        /*Session::flash('tipo','success');
        Session::flash('message', 'Departamento eliminado exitosamente');*/

    }
    public function index()
    {
        //
    }

    public function getDepartmentsByCountry(Request $request)
    {



        $department = DB::table('departments')
            ->where('departments.id_contry', '=', $request->input('id_country'))
            ->select('departments.*')->get();


        return response()->json(['success'=>true,'departmens'=> $department]);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
