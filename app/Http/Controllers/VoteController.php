<?php

namespace App\Http\Controllers;

use App\Usercommitteevote;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\Candidate;
class VoteController extends Controller
{
    public function setVote($idCandi, $iU){
        $objetoCandidato = DB::table('candidates')
            ->select('candidates.*')
            ->where('id', $idCandi)
            ->get();
        $idCommittee = $objetoCandidato[0]->id_committee;

        $check = DB::table('usercommitteevotes')->where('id_committee', $idCommittee)->get();

        if($check){
            return view('frontend.check');
        }else{
            $votesRecieved = $objetoCandidato[0]->votesRecieved;
            $votes = $votesRecieved+1;

            $CandidatoParaUpdate= Candidate::find($idCandi);
            $CandidatoParaUpdate->votesRecieved = $votes;
            $CandidatoParaUpdate->save();

            $hoy = date("Y:m-d H:i:s");

            $idUser = $iU;
            DB::table('usercommitteevotes')->insert(
                ['id_committee' => $idCommittee, 'id_user' => $idUser, 'created_at' => $hoy]
            );
            return redirect()->to('/chooseCommittee');
        }


        
    }
}
