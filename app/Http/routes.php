<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('home');
});

Route::auth();


use App\Http\Requests;
Route::group(['middleware' => 'auth'], function () {

    //aqui las rutas que solo usuarios logueados pueden ver

    Route::group(['middleware' => 'admin'], function () {

        //aqui rutas que solo administradores pueden ver


        Route::get('/contries', 'ContryController@getContry');
        Route::get('/contries/register', 'ContryController@getContryRegister');
        Route::post('/contries/setContry', 'ContryController@setContry');
        Route::get('/contries/deleteContry/{id}', 'ContryController@deleteContry');



        Route::get('/documents', 'DocumentController@getDocument');
        Route::get('/documents/register', 'DocumentController@getDocumentRegister');
        Route::post('/documents/setDocument', 'DocumentController@setDocument');
        Route::resource('Documents','DocumentController');


        Route::get('/departments', 'DepartmentController@getDepartment');
        Route::get('/departments/register', 'DepartmentController@getDepartmentRegister');
        Route::post('/departments/setDepartment', 'DepartmentController@setDepartment');
        Route::resource('Departments','DepartmentController');



        Route::get('/committees', 'CommitteesController@getCommittee');
        Route::get('/committees/register', 'CommitteesController@getCommitteeRegister');
        Route::post('/committees/setCommittee', 'CommitteesController@setCommittee');
        Route::resource('Committees', 'CommitteesController');


    });
    Route::get('/reports', 'ReportController@getReport');
    Route::post('/candidatesList','ListCandidatesController@getCandiates');
    Route::get('setVote/{id}/{idU}', 'VoteController@setVote');
    Route::get('/home', 'HomeController@index');
    Route::get('/chooseCommittee', function(){
        $committees = DB::table('committees')
            ->join('contries', 'committees.id_contry', '=', 'contries.id')
            ->select('contries.contryName','committees.committeeName', 'committees.id')->get();
        return view('frontend.chooseCommittee', compact('committees'));
    });
    
});




Route::get('/contries', 'ContryController@getContry');
Route::get('/contries/register', 'ContryController@getContryRegister');
Route::post('/contries/setContry', 'ContryController@setContry');
Route::delete('/contries/deleteContry/{id}', 'ContryController@deleteContry');
Route::get('/home', 'HomeController@index');

Route::get('/documents', 'DocumentController@getDocument');
Route::get('/documents/register', 'DocumentController@getDocumentRegister');
Route::post('/documents/setDocument', 'DocumentController@setDocument');

Route::get('/departments', 'DepartmentController@getDepartment');
Route::get('/departments/register', 'DepartmentController@getDepartmentRegister');
Route::post('/departments/setDepartment', 'DepartmentController@setDepartment');

Route::get('/committees', 'CommitteesController@getCommittee');
Route::get('/committees/register', 'CommitteesController@getCommitteeRegister');
Route::post('/committees/setCommittee', 'CommitteesController@setCommittee');

//CANDIDATES
//Route::get('/candidate/register', 'CandidateController@index');
Route::get('candidates', function(){
    return view('cms.candidate');
});
Route::resource('Candidates','CandidateController');
Route::get('/candidate/register', 'CandidateController@registerCandidate');
Route::get('/candidate/Updateregister/{id}', 'CandidateController@UpdateCandidate');
Route::post('/candidate/getDepartments', 'CandidateController@getDepartmentsByCountry')->name('getDepartments');

