<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usercommitteevote extends Model
{
    protected $fillable = ['id_committee', 'id_user'];
}
